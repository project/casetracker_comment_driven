
Introduction
------------
casetracker_comment_driven (CTCD for short) allows for CCK fields to be added to Case Tracker and tracked in the comments in a manner identical to how standard cases are tracked. This is achieved by using the Comment Driven module to track changes made with each comment and merging the Case Tracker table with the Comment Driven table to make them appear as one.

This method should work for anything that can be tracked with comment_driven. Hypothetically, this includes the title, taxonomy terms (tags) and almost any other field. This functionality has not been tested yet.


Installation
------------

 * Download and enable all dependencies.
 * Download and enable casetracker_comment_driven.


Setup
-----

 * Add CCK fields to the castracker_basic_case type.
 * Visit *Admin > Content Management > Content Types > castracker_basic_case > Comment Driven*.
 * Check the box *Enable driven properties*.
 * Enable comment_driven for any CCK field that you would like to see tracked.


Limitations
-----------
Currently, this module only integrates with the casetracker_basic_case type instead of being able to integrate with any casetracker case. 


Todo
----
Immediate:

 * Make sure that a node has casetracker and comment driven enabled before merging comment diff tables and comment forms.
 
Later:
 
 * Make the merging work with any case type
 * Merge the comment forms.
 * Make the comment table merger work with comment_driven "live view"
 

Development
-----------
This module was initially developed for [Google Summer of Code 2010](http://drupal.org/node/782294) by [auzigog](http://drupal.org/user/542166) (Jeremy Blanchard). Further development is occurring as part of a set of grassroots project management tools being developed at [Activism Labs](http://activismlabs.org).

http://activismlabs.org
